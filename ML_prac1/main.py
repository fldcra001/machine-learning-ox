import cPickle as cp
import numpy as np

import matplotlib.pyplot as plt




X, y = cp.load(open('winequality-white.cPickle','rb'))

N, D = X.shape
N_train = int(0.8 * N)
N_test = N - N_train

X_train = X[:N_train]
y_train = y[:N_train]
X_test = X[N_train:]
y_test = y[N_train:]


#sample std dev
stdDevY = np.std(y, ddof=1)
y_train_mean = y_train.mean()

print 'There are ', N, ' total datapoints and ', D, 'dimensions in the full data set'
print 'There are ', N_train, ' training datapoints'
print 'There are ', N_test, ' test datapoints'

def computeMSE(arr, mean):
	total = 0
	numInputs = len(arr)
	for currentValue in arr:
		total += (currentValue - mean) ** 2
	return total / numInputs
	

def computeMSE2(hatArr, origArr):
	total = 0
	numInputs = len(hatArr)
	for i in range (0, numInputs):
		total += (hatArr[i] - origArr[i]) ** 2
	return total / numInputs

plt.hist(y_train)
plt.xlabel('Score')
plt.ylabel('Total')
plt.title('Distribution of wine scores')

plt.show()

y_train_MSE = computeMSE(y_train, y_train_mean)
print '\nThe MSE for the training data is: ', y_train_MSE

y_test_MSE = computeMSE(y_test, y_train_mean)
print 'The MSE for the test data is: ', y_test_MSE

## Standardise the data ## 

# axis=0 - normalises each column
X_train_std = (X_train - np.mean(X_train, axis=0)) / np.std(X_train, axis=0)

# standardise text data as well 
X_test_std = (X_test - np.mean(X_train, axis=0)) / np.std(X_test, axis=0)

# Add bias column to standardised X_train
X_train_std = np.c_[np.ones(N_train), X_train]
X_test_std = np.c_[np.ones(N_test), X_test]


# Compute weights

# w is a (D + 1) x 1 matrix
# w = (Xt X)^-1 Xt y
#inverse_X_train = np.linalg.inv(X_train)
XtX = (X_train_std.T).dot(X_train_std)
XtX_inverse = np.linalg.inv(XtX)
XtX_inverse_Xt = XtX_inverse.dot(X_train_std.T)

w = XtX_inverse_Xt.dot(y_train)
#print w

# now we can apply the model
# y_hat = X.w

y_train_hat = X_train_std.dot(w)

y_train_hat_MSE = computeMSE2(y_train_hat, y_train)
print '\nThe MSE using the linear model for the training data is: ', y_train_hat_MSE

y_test_hat = X_test_std.dot(w)

y_test_hat_MSE = computeMSE2(y_test_hat, y_test)
print 'The MSE using the linear model for the test data is: ', y_test_hat_MSE

###################################################################################

# LEARNING CURVES
#plot1, = plt.plot([], [])
xData = []
yTrainData = []
yTestData = []

# clear the current plot data
plt.clf()

for n in xrange(20, 600, 20):
	N, D = X.shape
	N_train = n
	N_test = N - N_train

	X_train = X[:N_train]
	y_train = y[:N_train]
	X_test = X[N_train:]
	y_test = y[N_train:]


	#sample std dev
	stdDevY = np.std(y, ddof=1)
	y_train_mean = y_train.mean()

	## Standardise the data ## 

	# axis=0 - normalises each column
	X_train_std = (X_train - np.mean(X_train, axis=0)) / np.std(X_train, axis=0)
	#print np.mean(X_train, axis = 0)
	#print np.mean(X_train_std, axis = 0)

	# standardise text data as well 
	X_test_std = (X_test - np.mean(X_train, axis=0)) / np.std(X_test, axis=0)

	# Add bias column to standardised X_train
	X_train_std = np.c_[np.ones(N_train), X_train]
	X_test_std = np.c_[np.ones(N_test), X_test]
	#print X_train[1]

	# Compute weights

	# w is a (D + 1) x 1 matrix
	# w = (Xt X)^-1 Xt y
	#inverse_X_train = np.linalg.inv(X_train)
	XtX = (X_train_std.T).dot(X_train_std)
	XtX_inverse = np.linalg.inv(XtX)
	XtX_inverse_Xt = XtX_inverse.dot(X_train_std.T)

	w = XtX_inverse_Xt.dot(y_train)
	#print w

	# now we can apply the model
	# y_hat = X.w

	y_train_hat = X_train_std.dot(w)

	y_train_hat_MSE = computeMSE2(y_train_hat, y_train)
	print '\nThe MSE using the linear model for the training data is: ', y_train_hat_MSE

	y_test_hat = X_test_std.dot(w)

	y_test_hat_MSE = computeMSE2(y_test_hat, y_test)
	print 'The MSE using the linear model for the test data is: ', y_test_hat_MSE
	
	xData.append(n)
	yTrainData.append(y_train_hat_MSE)
	yTestData.append(y_test_hat_MSE)
	#plot1.set_xdata(np.append(plot1.get_xdata(), n))
	#plot1.set_ydata(np.append(plot1.get_ydata(), y_train_hat_MSE))

plt.plot(xData, yTrainData, 'b-', label='Training error')
plt.plot(xData, yTestData, 'g-', label='Test error')
plt.legend()

plt.show()

N, D = X.shape
N_train = int(0.8 * N)
N_test = N - N_train

X_train = X[:N_train]
y_train = y[:N_train]
X_test = X[N_train:]
y_test = y[N_train:]

# Set last 20% of training set for validation 
N_validation = int(0.8 * N_train)
X_validation = X[N_validation:]
y_validation = y[N_validation:]

min_MSE = 100000
optimal_lambda = 10**-5
optimal_degree = 2

for i in range(-5, 6):
    # now iterate through degrees
    for d in range(2,5):
        poly = PolynomialFeatures(d)
        X_new = poly.fit_transform(X)
        
        l = 10 ** i

        clf = Ridge(alpha=l)
        clf.fit(X_new, y)

        y_pred = clf.predict(poly.fit_transform(X_validation))

        ridge_MSE = computeMSE2(y_pred, y_validation)
        if ridge_MSE < min_MSE:
            min_MSE = min(min_MSE, ridge_MSE)
            optimal_lambda = l
            optimal_d = d
            print 'new optimal:'
        
        print ridge_MSE
    print ''

print 'Optimal lambda for Ridge Regression is 10^', optimal_lambda
print 'Optimal degree for Ridge Regression is ', optimal_degree
